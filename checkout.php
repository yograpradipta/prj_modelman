<?php
include_once "library/inc.library.php";

?>

<section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="span4">
                <div class="inner-heading">
                    <h2>Transaksi</h2>
                </div>
            </div>
            <div class="span8">
                <ul class="breadcrumb">
                    <li><a href="#"><i class="icon-home"></i></a></li>
                    <li> <i class="icon-angle-right"></i></li>
                    <li class="active">Transaksi </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section id="content">
    <div class="container">
        <div class="row">
            <div class="span12" id="datapesan">
                <div class="row" style="margin:0">
                    <br>
                    <div class="span12" style="margin:0; background-color:#dcdcdc">
                        <h6 style="margin:0"><strong>Data</strong>Pesanan</h6>
                    </div>

                </div>

                <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="table-list">
                    <tr>
                        <td width="25" align="center" bgcolor="#F5F5F5"><strong>No</strong></td>
                        <td width="913" height="22" bgcolor="#F5F5F5"><strong>Nama Barang </strong></td>
                        <td width="129" align="right" bgcolor="#F5F5F5"><strong>Harga (Rp)</strong></td>
                        <td width="66" align="center" bgcolor="#F5F5F5"><strong>Jumlah</strong></td>
                        <td width="128" align="right" bgcolor="#F5F5F5"><strong>Total (Rp)</strong></td>
                    </tr>
                    <?php
                    // buat variabel data
                    $subTotal	= 0;
                    $totalHarga	= 0;
                    $totalBarang = 0;

                // Menampilkan daftar barang yang sudah dipilih (ada d Keranjang)
                    $mySql = "SELECT t_keranjang.tanggal, t_keranjang.quantity, t_keranjang.id, 
                                t_produk.nama_produk, t_produk.foto, t_produk.harga,t_produk.foto 
                                from t_keranjang 
                                LEFT JOIN t_produk on t_keranjang.id_produk=t_produk.id 
                                WHERE t_keranjang.id_user='$_SESSION[idmbr]' 
                                ORDER BY t_keranjang.id DESC";
                    $myQry = mysql_query($mySql);
                    $nomor	= 0;
                    while ($myData = mysql_fetch_array($myQry)) {
                    $nomor++;
                    // Mendapatkan total harga (harga * jumlah)
                    $subTotal= $myData['harga'] * $myData['quantity']; 
                    
                    // Mendapatkan total harga  dari seluruh  barang
                    $totalHarga = $totalHarga + $subTotal; 
                    
                    // Mendapatkan total barang
                    $totalBarang = $totalBarang + $myData['quantity']; 
                ?>
                    <tr>
                        <td align="center"><?php echo $nomor; ?></td>
                        <td><a href="?open=Barang-Lihat&amp;Kode=<?php echo $myData['kd_barang']; ?>"
                                target="_blank"><?php echo $myData['nama_produk']; ?></a></td>
                        <td align="right">Rp.<?php echo format_angka($myData['harga']); ?></td>
                        <td align="center"><?php echo $myData['quantity']; ?></td>
                        <td align="right">Rp. <?php echo format_angka($subTotal); ?></td>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td colspan="3" align="right"><b>GRAND TOTAL BELANJA :</b></td>
                        <td align="center" bgcolor="#F5F5F5"><?php echo $totalBarang; ?></td>
                        <td align="right" bgcolor="#F5F5F5"><strong>Rp.
                                <?php echo format_angka($totalHarga); ?></strong></td>
                    </tr>
                </table>
            </div>
            <br>
            <div class="span12" style="margin-top:20px; background-color:#dcdcdc">
                <h6 style="margin:0"><strong>Data</strong>Transaksi</h6>
            </div>
            <div class="span12 ">
                <form action="" method="post" name="formboking" role="form" enctype="multipart/form-data"
                    class="contactForm">
                    <div class="row">
                        <div class="span6 form-group">
                            
                            <div class="validation"></div>
                            Tanggal Booking <br>
                            <input type="date" name="tglbooking" /><br>

<!--  -->
                            <!-- <table width="100%">
                                <tr >
                                    <td>
                                        Pilih Kecamatan <br>
                                        <?php
                                        //     $result = mysql_query("select * from t_ongkir");
                                        //     $jsArray = "var kota = new Array();\n";
                                        //     echo '<select name="ongkir" onchange="changeKota(this.value)">';
                                        //     echo '<option>Pilih kecamatan</option>';
                                        //     while ($row = mysql_fetch_array($result)) {
                                        //         echo '<option value="' . $row['ongkir'] . '">' . $row['kecamatan'] . '</option>';
                                        //         $jsArray .= "kota['" . $row['ongkir'] . "'] = {ongkir:'" . addslashes($row['ongkir']) . "'};\n";  
                                        //     }
                                        //     echo '</select>';
                                        // ?>
                                        
                                    </td>
                                    <td>
                                        ongkir: <input type="text" readonly="true" name="ongkir" id="ongkir" />
                                        <script type="text/javascript">
                                            <?php/*  echo $jsArray; */ ?>
                                            function changeKota(id) {
                                                document.getElementById('ongkir').value = "Rp."+kota[id].ongkir;
                                            };
                                        </script>
                                    </td>
                                    
                                    
                                </tr>
                            </table> -->
<!--  -->
                            Tanggal Transfer <br>
                            <input style="width:95%" type="date" name="tgltrans" /><br>
                            <div class="validation"></div>
                            Nama Bank Pengirim<br>
                            <input type="text" name="bankpengirim" />
                            <div class="validation"></div>
                            Atas Nama Pengirim<br>
                            <input type="text" name="pengirim" />
                            <div class="validation"></div>
                            Nomor rekening pengirim<br>
                            <input type="number" style="width: 100%;
                                min-height: 40px;
                                padding-left: 20px;
                                font-size: 13px;
                                padding-right: 20px;
                                -webkit-box-sizing: border-box;
                                -moz-box-sizing: border-box;
                                box-sizing: border-box;" name="norek" />
                            <div class="validation"></div>
                            Jumlah transfer  <br>
                            <input type="number" style="width:100%; min-height: 40px;
                                padding-left: 20px;
                                font-size: 13px;
                                padding-right: 20px;
                                -webkit-box-sizing: border-box;
                                -moz-box-sizing: border-box;
                                box-sizing: border-box;" name="jumtrans"
                                placeholder="min setengah dari belanja"
                                />
                            Lokasi:
                            <textarea class="form-control" name="lokasi" style="max-height:112px" rows="12"
                                data-rule="required" data-msg="Please write something for us"
                                placeholder="ALamat Lengkap Pesta"></textarea>
                            <div class="validation"></div>
                        </div>
                        <div class="span6 form-group">
                            Bank Tujuan <br>
                            <?php
                                $result = mysql_query("select * from t_bank");
                                $jsArray = "var bank = new Array();\n";
                                echo '<select name="namabank" onchange="changeValue(this.value)">';
                                echo '<option>Pilih Bank</option>';
                                while ($row = mysql_fetch_array($result)) {
                                    echo '<option value="' . $row['idbank'] . '">' . $row['namabank'] . '</option>';
                                    $jsArray .= "bank['" . $row['idbank'] . "'] = {nomorrek:'" . addslashes($row['norek']) . "',an:'".addslashes($row['atasnama'])."'};\n";  
                                }
                                echo '</select>';
                            ?>
                            <br />
                            Nomor Rekening Tujuan: <input type="text" readonly="true" name="norek" id="norek" />
                            Atas Nama Tujuan: <input type="text" readonly="true" name="atasnama" id="atasnama" />
                            <script type="text/javascript">
                                <?php echo $jsArray; ?>
                                function
                                changeValue(id) {
                                document.getElementById('norek').value = bank[id].nomorrek;
                                document.getElementById('atasnama').value =bank[id].an;
                                };
                            </script>
                        </div>
                        <div class="span6 form-group">
                            Catatan:
                            <textarea class="form-control" name="message" style="max-height:112px" rows="12"
                                data-rule="required" data-msg="Please write something for us"
                                placeholder="Tambahan Pesan"></textarea>
                            <div class="validation"></div>
                        </div>
                        <div class="span6 form-group">
                            Foto Bukti Transaksi: <br>
                            <input type="file" name="file_image">
                        </div>

                        <div class="span12">
                            <p class="text-center">
                                <button class="btn btn-large btn-theme margintop10 btn-rounded" name="checkout"
                                    type="submit">Checkout</button>
                            </p>
                        </div>
                    </div>
                </form>
                <?php
                    
                    $d = date("d");
                    $m = date("m");
                    $y = date("y");
                    $j = date("h");
                    $mi = date("i");
                    $s = date("s");
                    $kdboking = "BKig$y$m$d$j$mi$s";
                    if(isset($_POST["checkout"])){
                        $sisa= $totalHarga - $_POST['jumtrans'];
                        $lokasi_file    = $_FILES['file_image']['tmp_name'];
                        $tipe_file      = $_FILES['file_image']['type'];
                        $nama_file      = $_FILES['file_image']['name'];
                        $acak           = rand(000,999);
                        $nama_file_unik = $acak.$nama_file;
                        if(!empty($lokasi_file)){
                            
                            if($tipe_file =="image/jpeg" || $tipe_file == "image/png") {
                                move_uploaded_file($lokasi_file, "admin2/foto_bukti/$nama_file_unik");
                                $simpanCheckout=mysql_query("INSERT INTO t_checkout(kode_pesanan,tgl_boking,id_pelanggan,id_bank,totbay,
                                        norek_pengirim,atas_nama,tanggal_transfer, jumlah_transfer,sisbay,
                                        status,bukti,catatan, lokasi_pesta)
                                        VALUES ('$kdboking','$_POST[tglbooking]','$_SESSION[idmbr]','$_POST[namabank]','$totalHarga',
                                        '$_POST[norek]', '$_POST[pengirim]','$_POST[tgltrans]','$_POST[jumtrans]','$sisa',
                                        'diproses','$nama_file_unik','$_POST[message]','$_POST[lokasi]')");
                                if ($simpanCheckout) {
                                    $bacaSql= "SELECT t_keranjang.*, t_produk.harga, t_produk.kode,
                                    t_produk.nama_produk, t_produk.kategori, t_produk.foto, 
                                    t_pelanggan.nama, t_pelanggan.alamat, t_pelanggan.notelp
                                    FROM t_keranjang 
                                    LEFT JOIN t_produk ON t_produk.id = t_keranjang.id_produk 
                                    LEFT JOIN t_pelanggan ON t_pelanggan.id = t_keranjang.id_user 
                                    WHERE t_keranjang.id_user='$_SESSION[idmbr]'";
                                    $bacaQry	= mysql_query($bacaSql);
                                    while ($bacaData = mysql_fetch_array($bacaQry)) {
                                        // Simpan data dari Keranjang belanja ke Pemesanan_Item
                                        $idProduk 	= $bacaData['id_produk'];
                                        $Harga	= $bacaData['harga'];
                                        $Jumlah	= $bacaData['quantity'];
                                        
                                        $simpanDetail="INSERT INTO t_detailpesanan(kode_pesanan, 
                                                    id_user,id_produk,quantity,harga) 
                                                    VALUES('$kdboking','$_SESSION[idmbr]', 
                                                    '$idProduk', '$Jumlah', '$Harga')";
                                        mysql_query($simpanDetail);
                                        $updateQry="UPDATE `t_produk` SET `stok` = (stok - $Jumlah) WHERE `t_produk`.`id` = '$idProduk'";
                                        mysql_query($updateQry);
                                    };
                                    mysql_query("Delete from t_keranjang WHERE id_user = '$_SESSION[idmbr]'");
                                }
                                
                                echo "<script>alert('data terkirim, pesanan sedang diproses ')</script>";
                                echo "<META HTTP-EQUIV='Refresh' Content='3; URL=?page=daftar'>";
                            }else{
                                echo "Gagal menyimpan data !!! <br>
                                    Tipe file <b>$nama_file</b> : $tipe_file <br>
                                    Tipe file yang diperbolehkan adalah : <b>JPG/JPEG</b>.<br>";
                                echo "<a href='javascript:history.go(-1)'>Ulangi Lagi</a>";
                            }
                        }else{
                            $simpanCheckout= mysql_query("INSERT INTO t_checkout(kode_pesanan,tgl_boking,id_pelanggan,id_bank,totbay,
                                norek_pengirim,atas_nama,tanggal_transfer, jumlah_transfer,sisbay,
                                status,catatan, lokasi_pesta)
                                VALUES ('$kdboking','$_POST[tglbooking]','$_SESSION[idmbr]','$_POST[namabank]','$totalHarga',
                                '$_POST[norek]', '$_POST[pengirim]','$_POST[tgltrans]','$_POST[jumtrans]','$sisa',
                                'diproses','$_POST[message]','$_POST[lokasi]')");
                                if ($simpanCheckout) {
                                    $bacaSql= "SELECT t_keranjang.*, t_produk.harga, t_produk.kode,
                                    t_produk.nama_produk, t_produk.kategori, t_produk.foto, 
                                    t_pelanggan.nama, t_pelanggan.alamat, t_pelanggan.notelp
                                    FROM t_keranjang 
                                    LEFT JOIN t_produk ON t_produk.id = t_keranjang.id_produk 
                                    LEFT JOIN t_pelanggan ON t_pelanggan.id = t_keranjang.id_user 
                                    WHERE t_keranjang.id_user='$_SESSION[idmbr]'";
                                    $bacaQry	= mysql_query($bacaSql);
                                    while ($bacaData = mysql_fetch_array($bacaQry)) {
                                        // Simpan data dari Keranjang belanja ke Pemesanan_Item
                                        $idProduk 	= $bacaData['id_produk'];
                                        $Harga	= $bacaData['harga'];
                                        $Jumlah	= $bacaData['quantity'];
                                        
                                        $simpanDetail="INSERT INTO t_detailpesanan(kode_pesanan, 
                                                    id_user,id_produk,quantity,harga) 
                                                    VALUES('$kdboking','$_SESSION[idmbr]', 
                                                    '$idProduk', '$Jumlah', '$Harga')";
                                        mysql_query($simpanDetail);
                                    }
                                    mysql_query("Delete from t_keranjang WHERE id_user = '$_SESSION[idmbr]'");
                                }
                            
                            echo "<script>alert('data terkirim, pesanan sedang diproses ')</script>";
                            echo "<META HTTP-EQUIV='Refresh' Content='3; URL=?page=daftar'>";
                        }
                    }
                    ?>
            </div>
        </div>
    </div>
</section>
<!-- </div> -->