<section id="featured">
      <!-- start slider -->
      <!-- Slider -->
      <div id="nivo-slider">
        <div class="nivo-slider">
          <!-- Slide #1 image -->
          <img src="img/slides/nivo/pkl-1.jpg" alt="" title="#caption-1" />
          <!-- Slide #2 image -->
          <img src="img/slides/nivo/pkl-2.jpg" alt="" title="#caption-2" />
          <!-- Slide #3 image -->
          <img src="img/slides/nivo/pkl-3.jpg" alt="" title="#caption-3" />
        </div>
        <div class="container">
          <div class="row">
            <div class="span12">
              <!-- Slide #1 caption -->
              <div class="nivo-caption" id="caption-1">
                <div>
                  <h2> <strong>Tenda</strong></h2>
                  <p>
                    menyediakan penyewaan berbagai model tenda pernikahan  
                  </p>
                  <a href="#" class="btn btn-theme">Lihat</a>
                </div>
              </div>
              <!-- Slide #2 caption -->
              <div class="nivo-caption" id="caption-2">
                <div>
                  <h2>Pakaian <strong>Pengantin</strong></h2>
                  <p>
                    Menyediakan penyewaan berbagai macam model pakaian pengantin</p>
                  <a href="#" class="btn btn-theme">Lihat</a>
                </div>
              </div>
              <!-- Slide #3 caption -->
              <div class="nivo-caption" id="caption-3">
                <div>
                  <h2>Jasa <strong>Make UP</strong></h2>
                  <p>
                    Menyediakan Jasa Make Up dengan kualitas terbaik</p>
                  <a href="#" class="btn btn-theme">Lihat</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end slider -->
    </section>
    <section class="callaction">
      <div class="container">
        <div class="row">
          <div class="span12">
            <div class="big-cta">
              <div class="cta-text">
                <h3>Kami Menyediakan <span class="highlight"><strong>Paket</strong></span> Lengkap Disini!</h3>
              </div>
              <div class="cta floatright">
                <a class="btn btn-large btn-theme btn-rounded" href="?page=produk&kategori='paket'">View More Package</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="content">
      <div class="container">
        <div class="row">
          <div class="span12">
            <div class="row">
              <div class="span3">
                <div class="box aligncenter">
                  <div class="aligncenter icon">
                    <i class="icon-eject icon-circled icon-64 active"></i>
                  </div>
                  <div class="text">
                    <h6>Pelaminan</h6>
                    <p>
                      menyediakan berbagai macam bentuk pelaminan.
                    </p>
                    <a href="?page=produk&kategori='lamin'">lihat</a>
                  </div>
                </div>
              </div>
              <div class="span3">
                <div class="box aligncenter">
                  <div class="aligncenter icon">
                    <i class="font-icon-home icon-circled icon-64 active"></i>
                  </div>
                  <div class="text">
                    <h6>Tenda</h6>
                    <p>
                      menyediakan penyewaan berbagai macam bentuk tenda.
                    </p>
                    <a href="?page=produk&kategori='tenda'">Lihat</a>
                  </div>
                </div>
              </div>
              <div class="span3">
                <div class="box aligncenter">
                  <div class="aligncenter icon">
                    <i class="icon-user icon-circled icon-64 active"></i>
                  </div>
                  <div class="text">
                    <h6>Baju</h6>
                    <p>
                      menyediakan sewa berbagai macam baju pengantin.
                    </p>
                    <a href="?page=produk&kategori='baju'">Lihat</a>
                  </div>
                </div>
              </div>
              <div class="span3">
                <div class="box aligncenter">
                  <div class="aligncenter icon">
                    <i class="font-icon-magic icon-circled icon-64 active"></i>
                  </div>
                  <div class="text">
                    <h6>Makeup</h6>
                    <p>
                      menyediakan jasa makeup yang berkualitas.
                    </p>
                    <a href="makeup">Lihat</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- divider -->
        <div class="row">
          <div class="span12">
            <div class="solidline">
            </div>
          </div>
        </div>
        <!-- end divider -->
        <!-- Portfolio Projects -->
        <div class="row">
          <div class="span12">
            <h4 class="heading">Layanan Pelaminan <strong>Modelman</strong></h4>
            <ul class="portfolio-categ filter">
              <li class="all active"><a href="#">Semua</a></li>
              <li class="lamin"><a href="#">Lamin</a></li>
              <li class="baju"><a href="#" title="">Baju</a></li>
              <li class="tenda"><a href="#" title="">Tenda</a></li>
              <li class="makeup"><a href="#" title="">Makeup</a></li>
            </ul>
            <div class="clearfix">
            </div>
            <div class="row">
              <section id="projects">
                <ul id="thumbs" class="portfolio">
                  <!-- Item Project and Filter Name -->
                  <?php
                    include_once ("admin2/koneksi.php");
                    $no=0;
                    $data=mysql_query("SELECT * FROM t_produk WHERE kategori !='paket' ORDER BY rand() LIMIT 4");
                    while($ar=mysql_fetch_array($data)){
                  ?>
                    <li class="item-thumbs span3 photography" data-id="id-<?php echo $no; ?>" data-type=<?php echo $ar['kategori']; ?>>
                        <!-- Fancybox - Gallery Enabled - Title - Full Image -->
                        <a class="hover-wrap fancybox" data-fancybox-group="gallery" title=<?php echo $ar['nama_produk']; ?> href="admin2/foto_produk/<?php echo $ar['foto']; ?>">
                            <span class="overlay-img"></span>
                            <span class="overlay-img-thumb font-icon-plus"></span>
                        </a>
                        <!-- Thumb Image and Description -->
                        <img src="admin2/foto_produk/<?php echo $ar['foto']; ?>" alt="<?php echo $ar['keterangan']; ?>">
                        <!-- <a class="btn btn-info btn-rounded" href="#"><i class="icon-edit"></i> view</a> -->
                    </li>
                  <?php
                  $no++;
                    }
                    ?>
                  <!-- End Item Project -->
                  
                </ul>
              </section>
            </div>
          </div>
        </div>
        <!-- End Portfolio Projects -->
        <!-- divider -->
        <div class="row">
          <div class="span12">
            <div class="solidline">
            </div>
          </div>
        </div>
        <!-- end divider -->
        <div class="row">
          <div class="span12">
            <h4>Very satisfied <strong>clients</strong></h4>
            <ul id="mycarousel" class="jcarousel-skin-tango recent-jcarousel clients">
              <li>
                <a href="#">
					<img src="img/dummies/clients/client1.png" class="client-logo" alt="" />
					</a>
              </li>
              <li>
                <a href="#">
					<img src="img/dummies/clients/client2.png" class="client-logo" alt="" />
					</a>
              </li>
              <li>
                <a href="#">
					<img src="img/dummies/clients/client3.png" class="client-logo" alt="" />
					</a>
              </li>
              <li>
                <a href="#">
					<img src="img/dummies/clients/client4.png" class="client-logo" alt="" />
					</a>
              </li>
              <li>
                <a href="#">
					<img src="img/dummies/clients/client5.png" class="client-logo" alt="" />
					</a>
              </li>
              <li>
                <a href="#">
					<img src="img/dummies/clients/client6.png" class="client-logo" alt="" />
					</a>
              </li>
              <li>
                <a href="#">
					<img src="img/dummies/clients/client1.png" class="client-logo" alt="" />
					</a>
              </li>
              <li>
                <a href="#">
					<img src="img/dummies/clients/client2.png" class="client-logo" alt="" />
					</a>
              </li>
              <li>
                <a href="#">
					<img src="img/dummies/clients/client3.png" class="client-logo" alt="" />
					</a>
              </li>
              <li>
                <a href="#">
					<img src="img/dummies/clients/client4.png" class="client-logo" alt="" />
					</a>
              </li>
              <li>
                <a href="#">
					<img src="img/dummies/clients/client5.png" class="client-logo" alt="" />
					</a>
              </li>
              <li>
                <a href="#">
					<img src="img/dummies/clients/client6.png" class="client-logo" alt="" />
					</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>