<?php
session_start();
include_once "library/inc.library.php";    // Membuka librari peringah fungsi
include_once ("admin2/koneksi.php");

if(isset($_GET['kode'])) {
	// Membaca Kode dari URL
	$Kode	= $_GET['kode'];
	
	// Query membaca data Utama Pemesanan 
	$ambilCheckout = "SELECT
    t_checkout.*,
    t_bank.namabank,
    t_bank.atasnama as penerima,
    t_bank.norek,
    t_bank.logobank,
    t_pelanggan.nama,
    t_pelanggan.noktp,
    t_pelanggan.notelp,
    t_pelanggan.alamat
  FROM
    t_checkout
  LEFT JOIN
    t_pelanggan ON t_pelanggan.id = t_checkout.id_pelanggan
  LEFT JOIN
    t_bank ON t_bank.idbank = t_checkout.id_bank
  WHERE t_checkout.kode_pesanan='$Kode' ";
	$myQry = mysql_query($ambilCheckout);
	$myData = mysql_fetch_array($myQry);
}
	echo"<body onLoad='javascript:window:print()'>";
?>
<html >
    <head>
        <title>Data Detil Pesanan</title>
        <link href="style/styles_cetak.css" rel="stylesheet" type="text/css">
        <link href="style/button.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <h1>CHECKOUT PEMESANAN </h1>
        <table width="550" border="0" cellspacing="2" cellpadding="3">
            <tr>
                <td bgcolor="#CCCCCC"><strong>TRANSAKSI</strong></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td width="31%"><b>Kode Pemesanan</b></td>
                <td width="2%">:</td>
                <td width="67%"><?php echo $myData['kode_pesanan']; ?></td>
            </tr>
            <tr>
                <td><b>Tanggal Transaksi</b></td>
                <td>:</td>
                <td><?php echo IndonesiaTgl($myData['tanggal']); ?></td>
            </tr>
            <tr>
                <td><b>Nama Pelanggan</b></td>
                <td>:</td>
                <td><?php echo $myData['nama']; ?></td>
            </tr>
            <tr>
                <td><b>Tanggal Booking</b></td>
                <td>:</td>
                <td><?php echo IndonesiaTgl($myData['tgl_boking']); ?></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td bgcolor="#CCCCCC"><strong>TRANSFER</strong></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td width="31%"><b>Norek Pengirim</b></td>
                <td width="2%">:</td>
                <td width="67%"><?php echo $myData['norek_pengirim']; ?></td>
            </tr>
            <tr>
                <td><b>Atas Nama</b></td>
                <td>:</td>
                <td><?php echo $myData['atas_nama']; ?></td>
            </tr>
            <tr>
                <td><b>Tanggal Transfer</b></td>
                <td>:</td>
                <td><?php echo IndonesiaTgl($myData['tanggal_transfer']); ?></td>
            </tr>
            <tr>
                <td><b>Jumlah Transfer</b></td>
                <td>:</td>
                <td><?php echo format_angka($myData['jumlah_transfer']); ?></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>


            <tr>
                <td bgcolor="#CCCCCC"><strong>PENERIMA</strong></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><b>Nama Penerima</b></td>
                <td>:</td>
                <td><?php echo $myData['penerima']; ?></td>
            </tr>
            <tr>
                <td><b>BANK Penerima</b></td>
                <td>:</td>
                <td><?php echo $myData['namabank']; ?></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td bgcolor="#FFFF99"><b>Status Pembayaran </b></td>
                <td>:</td>
                <td><?php echo $myData['status']; ?> * </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <h2>DAFTAR PESANAN BARANG</h2>
        <table width="761" border="0" cellpadding="2" cellspacing="0">
            <tr>
                <td width="23" align="center" bgcolor="#CCCCCC"><strong>No</strong></td>
                <td width="324" bgcolor="#CCCCCC"><strong>Nama Barang </strong></td>
                <td width="60" align="center" bgcolor="#CCCCCC"><strong>Jumlah</strong></td>
                <td width="200" align="right" bgcolor="#CCCCCC"><strong>Harga (Rp)</strong></td>
                <td width="150" align="right" bgcolor="#CCCCCC"><strong>Total (Rp)</strong></td>
            </tr>
            <?php 
                // Deklarasi variabel
                $subTotal	= 0;
                $totalBarang = 0;
                $totalBiayaKirim = 0;
                $totalHarga = 0;
                $totalBayar =0;
                $unik_transfer =0;
                
                // SQL Menampilkan data Barang yang dipesan
                $tampilSql = "SELECT
                                t_detailpesanan.quantity,
                                t_produk.nama_produk,
                                t_produk.harga,
                                t_produk.kategori,
                                (
                                t_detailpesanan.quantity * t_produk.harga
                                ) AS total
                            FROM
                                t_detailpesanan
                            LEFT JOIN
                                t_produk ON t_produk.id = t_detailpesanan.id_produk
                            WHERE
                                t_detailpesanan.kode_pesanan = '$Kode'
                            ORDER BY
                                t_detailpesanan.id DESC";
                $tampilQry = mysql_query($tampilSql);
                $no	= 0; 
                while ($tampilData = mysql_fetch_array($tampilQry)) {
                $no++;
                // Menghitung subtotal harga (harga  * jumlah)
                // $subTotal		= $tampilData['harga'] * $tampilData['jumlah'];
                
                // Menjumlah total semua harga 
                $totalHarga 	= $totalHarga + $tampilData['total'];  
                
                // Menjumlah item barang
                // $totalBarang	= $totalBarang + $tampilData['jumlah'];
            ?>
            <tr>
                <td align="center"><?php echo $no; ?></td>
                <td><?php echo $tampilData['nama_produk']; ?></td>
                <td><?php echo $tampilData['quantity']; ?></td>
                <td align="right">Rp. <?php echo format_angka($tampilData['harga']); ?></td>
                <td align="right">Rp. <?php echo format_angka($tampilData['total']); ?></td>
            </tr>
            <?php } 
                // Menghitung
                    // Total biaya Kirim = Biaya kirim x Total barang
                    // $totalBiayaKirim = $myData['biaya_kirim'] * $totalBarang;
                    
                    // $totalBayar = $totalHarga + $totalBiayaKirim;  
                    
                    // $digitHp 	= substr($myData['no_telepon'],-3); // ambil 3 digit terakhir no HP
                    // $unik_transfer = substr($totalBayar,0,-3).$digitHp;
            ?>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td align="right">&nbsp;</td>
            </tr>
            <!-- <tr>
                <td colspan="5" align="right" bgcolor="#F5F5F5"><strong>Total Belanja (Rp) : </strong></td>
                <td align="right" bgcolor="#F5F5F5">Rp. <?php 
                // echo format_angka($totalHarga); ?></td>
            </tr> -->
            <!-- <tr>
                <td colspan="5" align="right"><strong>Total Biaya Kirim  (Rp) : </strong></td>
                <td align="right">Rp. <?php 
                // echo format_angka($totalBiayaKirim); ?></td>
            </tr> -->
            <tr>
                <td colspan="4" align="right" bgcolor="#F5F5F5"><strong>GRAND TOTAL  (Rp) : </strong></td>
                <td align="right" bgcolor="#F5F5F5"><?php echo format_angka($totalHarga); ?></td>
            </tr>
            <!-- <tr>
                <td colspan="6" align="right">Nominal pembayarannya adalah <b>Rp. <?php echo format_angka($unik_transfer); ?></b> </td>
            </tr> -->
            </table>
        <p><b>* Keterangan Status Pembayaran :</b></p>
        <ul>
            <li><b>diproses :</b> Masih Dicek Oleh <strong>Admin</strong>.</li>
            <li><b>Konfirmasi :</b> Pembayaran telah disetujui oleh admin, dan <strong>Dalam Proses Pengiriman</strong>.</li>
            <li><b>Cancel :</b> Pemesanan batal.     </li>
        </ul>
    </body>
</html>