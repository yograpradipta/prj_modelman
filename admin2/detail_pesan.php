<!-- Page Header-->
<header class="page-header">
    <div class="container-fluid">
        <h2 class="no-margin-bottom">Detail Pesana</h2>
    </div>
</header>
<div class="container">
    <section >
        <?php
        include_once "../library/inc.library.php";    // Membuka librari peringah fungsi
        include_once ("koneksi.php");
        $kode=$_GET['kode'];
        // $ambil= mysql_query("SELECT * FROM t_pesanan WHERE id=$_GET[id]");
        $ambil2 = mysql_query("SELECT
                t_checkout.*,
                t_bank.namabank,
                t_bank.atasnama as penerima,
                t_bank.norek,
                t_bank.logobank,
                t_pelanggan.nama,
                t_pelanggan.noktp,
                t_pelanggan.notelp,
                t_pelanggan.alamat
            FROM
                t_checkout
            LEFT JOIN
                t_pelanggan ON t_pelanggan.id = t_checkout.id_pelanggan
            LEFT JOIN
                t_bank ON t_bank.idbank = t_checkout.id_bank
            WHERE t_checkout.kode_pesanan='$kode' ");
        $r = mysql_fetch_array($ambil2);
        ?>
        <!-- start print -->
        <div id="section-to-print">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="container">
                                <div class="row wrap">
                                    <table>
                                        <tr>
                                            <td>Kode Pesanan</td>
                                            <td>: </td>
                                            <td><b>  <?php echo $r['kode_pesanan']; ?></b></td>
                                        </tr>
                                    </table>
                                    <table  class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th><b>No</th>
                                                <th><b>Produk</th>
                                                <th><b>Jumlah</th>
                                                <th style="text-align:right;"><b>Harga</th>
                                                <th style="text-align:right;"><b>Total(Rp)</th>
                                            </tr>
                                        </thead>
                                        <?php
                                            $subTotal	= 0;
                                            $totalBarang = 0;
                                            $totalBiayaKirim = 0;
                                            $totalHarga = 0;
                                            $totalBayar =0;
                                            $unik_transfer =0;
                                            
                                            // SQL Menampilkan data Barang yang dipesan
                                            $tampilSql = "SELECT
                                                            t_detailpesanan.quantity,
                                                            t_produk.nama_produk,
                                                            t_produk.harga,
                                                            t_produk.kategori,
                                                            (
                                                            t_detailpesanan.quantity * t_produk.harga
                                                            ) AS total
                                                        FROM
                                                            t_detailpesanan
                                                        LEFT JOIN
                                                            t_produk ON t_produk.id = t_detailpesanan.id_produk
                                                        WHERE
                                                            t_detailpesanan.kode_pesanan = '$kode'
                                                        ORDER BY
                                                            t_detailpesanan.id DESC";
                                            $tampilQry = mysql_query($tampilSql);
                                            $no	= 0; ?>
                                        <tbody><?php
                                            while ($tampilData = mysql_fetch_array($tampilQry)) {
                                            $no++;
                                            // Menghitung subtotal harga (harga  * jumlah)
                                            // $subTotal		= $tampilData['harga'] * $tampilData['jumlah'];
                                            
                                            // Menjumlah total semua harga 
                                            $totalHarga 	= $totalHarga + $tampilData['total'];  
                                        ?>
                                        <tr>
                                            <td align="center"><?php echo $no; ?></td>
                                            <td><?php echo $tampilData['nama_produk']; ?></td>
                                            <td><?php echo $tampilData['quantity']; ?></td>
                                            <td align="right">Rp. <?php echo format_angka($tampilData['harga']); ?></td>
                                            <td align="right">Rp. <?php echo format_angka($tampilData['total']); ?></td>
                                        </tr>
                                        <?php }; ?>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td align="right">&nbsp;</td>
                                        </tr>
                                        </tbody>
                                        <thead>
                                            <tr>
                                                <td colspan="4" align="right" bgcolor="#FAFAFA"><strong>GRAND TOTAL  (Rp) : </strong></td>
                                                <td align="right" bgcolor="#F5F5F5"><b>Rp.<?php echo format_angka($totalHarga); ?></b></td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4> Data Pesanan</h4>
                                </div>    
                                <div class="card-body">
                                    <table>
                                        <tr>
                                            <td>Tanggal Booking</td>
                                            <td>: </td>
                                            <td align="right"><b><?php echo IndonesiaTgl($r['tgl_boking']); ?> </b></td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Transfer</td>
                                            <td>: </td>
                                            <td align="right"><b><?php echo IndonesiaTgl($r['tanggal_transfer']); ?> </b></td>
                                        </tr>
                                        <tr>
                                            <td>Jumlah Transfer</td>
                                            <td>: </td>
                                            <td align="right"><b>Rp.<?php echo format_angka($r['jumlah_transfer']); ?> </b></td>
                                        </tr>
                                        <tr>
                                            <td>Sisa Bayar</td>
                                            <td>: </td>
                                            <td align="right"><b>Rp.<?php echo format_angka($r['sisbay']); ?> </b></td>
                                        </tr>
                                        <tr>
                                            <td>Alamat Lengkap Pesta</td>
                                            <td>: </td>
                                            <td><b><?php echo $r['lokasi_pesta']; ?> </b></td>
                                        </tr>
                                        <tr>
                                            <td>Pesan Tambahan</td>
                                            <td>: </td>
                                            <td><b><?php echo $r['catatan']; ?> </b></td>
                                        </tr>
                                    </table>
                                </div> 
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4> Data Pelanggan</h4>
                                </div>    
                                <div class="card-body">
                                    <table>
                                        <tr>
                                            <td>Nama Costumer</td>
                                            <td>: </td>
                                            <td><b><?php echo $r['nama']; ?> </b></td>
                                        </tr>
                                        <tr>
                                            <td>Alamat</td>
                                            <td>: </td>
                                            <td><b><?php echo $r['alamat']; ?> </b></td>
                                        </tr>
                                        <tr>
                                            <td>No HP</td>
                                            <td>: </td>
                                            <td><b><?php echo $r['notelp']; ?> </b></td>
                                        </tr>
                                        <tr>
                                            <td>No KTP</td>
                                            <td>: </td>
                                            <td><b><?php echo $r['noktp']; ?> </b></td>
                                        </tr>
                                    </table>
                                </div> 
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h4>Bukti Transaksi</h4>
                        </div>    
                        <div class="card-body">
                            <table align="center">
                                
                                <tr style="vertical-align: top; ">
                                    <td style="padding-top:20px">
                                        <img src="foto_bukti/<?php echo $r['bukti']; ?>" alt="foto bukti" width="350px"/>                               
                                    </td>
                                </tr>
                                <tr >
                                    <td align="right">
                                        <?php
                                        if ($r['status'] == 'diproses') {
                                            ?>  
                                            <form action="" enctype="multipart/form-data" method="post">
                                                <button name="confirm" class="btn btn-primary btn-sm  hide-to-print">Konfirmasi</button>                                        
                                                <button name="cancel" class="btn btn-danger btn-sm  hide-to-print">Tolak</button>                                        
                                            </form>
                                            <?php
                                            if (isset($_POST['confirm'])) {
                                                mysql_query("UPDATE `t_checkout` SET `status` = 'konfirmasi' WHERE `t_checkout`.`kode_pesanan` = '$kode'");
                                                echo "<script>alert('data Pesanan Dikonfirmasi')</script>";
                                                echo "<META HTTP-EQUIV='Refresh' Content='1' URL=?page='detail_pesanan&kode=$kode'>";
                                            };
                                            if (isset($_POST['cancel'])) {
                                                mysql_query("UPDATE `t_checkout` SET `status` = 'cancel' WHERE `t_checkout`.`kode_pesanan` = '$kode'");
                                                echo "<script>alert('data Pesanan Ditolak')</script>";
                                                echo "<META HTTP-EQUIV='Refresh' Content='1' URL=?page='detail_pesanan&kode=$kode'>";
                                            };
                                            ?>
                                        <?php };
                                            if ($r['status'] == 'konfirmasi') {
                                                ?>
                                                <i style="color:green" >
                                                Pesanan telah,
                                                <strong>Dikonfirmasi</strong></i>
                                    </td>
                                </tr>
                                        <?php
                                            };
                                            if ($r['status']== 'cancel') {
                                                # code...
                                                echo '<i style="color:red">
                                                Data Pesanan ,
                                                <strong>Ditolak</strong></i>';
                                            }
                                            if ($r['status'] == 'diproses') {
                                                echo '<br ><i class="hide-to-print" style="color:red">Periksa data transfer benar benar </i> <b class="hide-to-print" style="color:red">Asli!!</b>';
                                            }
                                        ?>
                            </table>
                            
                        </div> 
                    </div>
                </div>
            </div>
        </div>
            <!-- end print -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <a class="btn btn-rounded btn-danger hide-to-print " href="javascript:history.go(-1)">
                    <i class="icon-download"></i> back</a>  
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <a class="btn btn-rounded btn-primary hide-to-print " href="javascript:print('section-to-print')">
                    <i class="close-print"></i> print</a> 
                </div>
            </div>
            <script>
                // print(){
                //     windows.print();
                // },
                print(el) {
                    var restorepage = document.body.innerHTML;
                    var printcontent = document.getElementById(el).innerHTML;
                    document.body.innerHTML = printcontent;
                    window.print();
                    document.body.innerHTML = restorepage;
                }
            </script>
        </div>
    </section>
</div>