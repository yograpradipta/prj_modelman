<?php
    session_start();
    if(!isset($_SESSION['user'])){
        header('location:login.php');
    }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin Modelman</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="../ico/icon.png">
	 <!--/script TinyMCE-->
    <script type="text/javascript" src="plugins/tinymce/js/tinymce/tinymce.min.js"></script>
    <!--/script-->
  </head>
  <body>
    <div class="page home-page">
      <!-- Main Navbar-->
      <header class="header">
        <?php
          include('header.php');
        ?>
      </header>
        <div class="page-content d-flex align-items-stretch">
            <!-- Side Navbar -->
          <?php
            include('menu.php');
          ?>
            <div class="content-inner">
              
              <?php
                include('main.php');
              ?>
              <!-- Updates Section                                                -->
              <section class="updates no-padding-top">
              <!-- Page Footer-->
              <footer class="main-footer">
                <div class="container-fluid">
                  <div class="row">
                    <div class="col-sm-6">
                      <p></i>Yogra Pradipta &copy; 2019</p>
                    </div>
                    <div class="col-sm-6 text-right">
                      <p>CV.<a href="#" class="external">Modelman</a></p>
                     </div>
                  </div>
                </div>
              </footer>
            </div>
        </div>
    </div>
    <!-- Javascript files-->
    <script>
    tinymce.init({
        selector: '#keterangan',
        mode    : 'textareas',
        theme   : 'modern',
        height  : '300',
        plugins : [
            'advlist autolink link image lists charmap print  hr anchor pagebreak spellchecker',
            ' wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
            'save table contextmenu directionality emoticons template paste textcolor'
        ] ,
        content_css: 'css/content.css',
        toolbar : 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | ' +
        'bullist numlist outdent indent | link image | print  media fullpage | forecolor | backcolor emoticons'
        });
    </script>

	<script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper.js/umd/popper.min.js"> </script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="js/charts-home.js"></script>
    <!-- Main File-->
    <script src="js/front.js"></script>
  </body>
</html>
