<!-- Page Header-->
<header class="page-header">
    <div class="container-fluid">
        <h2 class="no-margin-bottom">Data Pelanggan</h2>
    </div>
</header>
<div class="container-fluid">
    <section class="dashboard-counts ">
        <?php
            include_once ("koneksi.php");
        ?>
            <div class="line-chart bg-white d-flex align-items-center justify-content-center has-shadow">
                <table  class="table table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>nama pelanggan </th>
                            <th>No KTP</th>
                            <th>username</th>
                            <th>password</th>
                            <th>alamat</th>
                            <th>no telepon</th>
                            <th>Tanggal Daftar</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <?php
                    $no=1;
                    $tampil=mysql_query("SELECT * FROM t_pelanggan ORDER BY id ASC");
                    while($data=mysql_fetch_array($tampil)) {
                        ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo $data['nama']; ?></td>
                            <td><?php echo $data['noktp']; ?></td>
                            <td><?php echo $data['username']; ?></td>
                            <td><?php echo $data['password']; ?></td>
                            <td><?php echo $data['alamat']; ?></td>
                            <td><?php echo $data['notelp']; ?></td>
                            <td><?php echo $data['tgldaftar']; ?></td>
                            <td>
                                <a href="aksi_pelanggan.php?id=<?php echo $data['id']?>"
                                            class="btn btn-danger btn-sm"
                                            onclick="return confirm('Yakin akan menghapus data ?');"><span class="glyphicon glyphicon-trash"></span>
                                                Delete</a>
                            </td>
                        </tr>
                        <?php
                        $no++;
                    }
                    ?>
                </table>
            </div>
    </section>
</div>
