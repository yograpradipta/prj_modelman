-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 15 Mar 2021 pada 11.51
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbpelaminan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL,
  `name` varchar(50) NOT NULL,
  `notelp` varchar(30) NOT NULL,
  `foto_admin` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `name`, `notelp`, `foto_admin`) VALUES
(2, 'uyung@gmail.com', '12345678', 'uyung', '08222655xxx', '26641IMG_20181001_200559_596.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_bank`
--

CREATE TABLE `t_bank` (
  `idbank` int(11) NOT NULL,
  `namabank` varchar(30) NOT NULL,
  `atasnama` varchar(20) NOT NULL,
  `norek` varchar(20) NOT NULL,
  `logobank` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_bank`
--

INSERT INTO `t_bank` (`idbank`, `namabank`, `atasnama`, `norek`, `logobank`) VALUES
(5, 'BRI', 'nurul', '554701014547532', '555572bri.jpg'),
(6, 'BNI', 'Nova', '456652352351', '606018bni.png'),
(7, 'mandiri', 'yogra', '4789014547532', '401458mndiri.png'),
(8, 'nagari', 'uyung', '554701014547532', '975646nagari.png'),
(9, 'mega', 'angga saputra', '9790014547532', '115539bank mega.jpg'),
(10, 'muamalat', 'Andrezky', '8976755513245678', '508514117950_620.jpg'),
(11, 'btn', 'naruto', '9786756543467', '277984bidik-nasabah-millenial-bank-btn-luncurkan-tabungan-siap.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_checkout`
--

CREATE TABLE `t_checkout` (
  `id` int(3) NOT NULL,
  `kode_pesanan` varchar(20) NOT NULL,
  `tgl_boking` date NOT NULL,
  `id_pelanggan` int(3) NOT NULL,
  `id_bank` varchar(20) NOT NULL,
  `totbay` double NOT NULL,
  `norek_pengirim` int(30) NOT NULL,
  `atas_nama` varchar(50) NOT NULL,
  `tanggal_transfer` date NOT NULL,
  `jumlah_transfer` double NOT NULL,
  `sisbay` double NOT NULL,
  `status` varchar(20) NOT NULL,
  `bukti` text NOT NULL,
  `catatan` text NOT NULL,
  `lokasi_pesta` text NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_checkout`
--

INSERT INTO `t_checkout` (`id`, `kode_pesanan`, `tgl_boking`, `id_pelanggan`, `id_bank`, `totbay`, `norek_pengirim`, `atas_nama`, `tanggal_transfer`, `jumlah_transfer`, `sisbay`, `status`, `bukti`, `catatan`, `lokasi_pesta`, `tanggal`) VALUES
(1, 'BKig210314034229', '0000-00-00', 7, '5', 11000000, 2147483647, 'deli', '2021-03-23', 11000000, 0, 'diproses', '60bukti_BNI.jpg', 'ini oesanan kemarin', '', '2021-03-14 14:42:29'),
(2, 'BKig210314041326', '2021-04-01', 7, '5', 6000000, 2147483647, 'NURUL', '2021-02-28', 11000000, -5000000, 'diproses', '596bukti_BNI.jpg', 'asdfghjk', '', '2021-03-14 15:13:26'),
(3, 'BKig210314042631', '2021-03-18', 7, '8', 2500000, 2147483647, 'nova', '2021-03-10', 1500000, 1000000, 'diproses', '312bukti_BRI.jpg', '', '', '2021-03-14 15:26:31'),
(4, 'BKig210314053008', '2021-03-31', 12, '8', 4500000, 2147483647, 'witri', '2021-03-23', 2500000, 2000000, 'diproses', '679bukti_BRI.jpg', '', '', '2021-03-14 16:30:08'),
(5, 'BKig210315021021', '2021-03-20', 13, '5', 6500000, 2147483647, 'dian', '2021-03-14', 5000000, 1500000, 'diproses', '143bukti_BRI.jpg', '', '', '2021-03-15 01:10:21'),
(6, 'BKig210315030306', '2021-03-28', 14, '5', 5000000, 2147483647, 'dani', '2021-03-23', 5000000, 0, 'konfirmasi', '399bukti_BRI.jpg', 'catatan sesuai permintaan pelanggan', 'steba', '2021-03-15 02:04:03'),
(7, 'BKig210315061823', '2021-03-22', 15, '5', 4500000, 2147483647, 'nia', '2021-03-17', 3000000, 1500000, 'konfirmasi', '726bukti_BRI.jpg', 'catatan', 'jati ', '2021-03-15 05:19:35');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_detailpesanan`
--

CREATE TABLE `t_detailpesanan` (
  `id` int(3) NOT NULL,
  `kode_pesanan` varchar(20) NOT NULL,
  `id_checkout` int(3) NOT NULL,
  `id_user` int(3) NOT NULL,
  `id_produk` int(3) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `quantity` int(4) NOT NULL,
  `harga` double NOT NULL,
  `total` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_detailpesanan`
--

INSERT INTO `t_detailpesanan` (`id`, `kode_pesanan`, `id_checkout`, `id_user`, `id_produk`, `tanggal`, `quantity`, `harga`, `total`) VALUES
(10, 'BKig210314034229', 0, 7, 3, '2021-03-14 14:42:29', 1, 7500000, 0),
(11, 'BKig210314034229', 0, 7, 6, '2021-03-14 14:42:29', 2, 1500000, 0),
(12, 'BKig210314034229', 0, 7, 13, '2021-03-14 14:42:29', 2, 250000, 0),
(13, 'BKig210314041326', 0, 7, 4, '2021-03-14 15:13:26', 2, 3000000, 0),
(14, 'BKig210314042631', 0, 7, 6, '2021-03-14 15:26:31', 1, 1500000, 0),
(15, 'BKig210314042631', 0, 7, 11, '2021-03-14 15:26:31', 1, 1000000, 0),
(16, 'BKig210314053008', 0, 12, 2, '2021-03-14 16:30:08', 1, 3000000, 0),
(17, 'BKig210314053008', 0, 12, 7, '2021-03-14 16:30:08', 1, 1500000, 0),
(18, 'BKig210315021021', 0, 13, 2, '2021-03-15 01:10:21', 1, 3000000, 0),
(19, 'BKig210315021021', 0, 13, 9, '2021-03-15 01:10:21', 1, 3500000, 0),
(20, 'BKig210315030306', 0, 14, 1, '2021-03-15 02:03:06', 1, 3500000, 0),
(21, 'BKig210315030306', 0, 14, 6, '2021-03-15 02:03:06', 1, 1500000, 0),
(22, 'BKig210315061823', 0, 15, 2, '2021-03-15 05:18:23', 1, 3000000, 0),
(23, 'BKig210315061823', 0, 15, 7, '2021-03-15 05:18:23', 1, 1500000, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_keranjang`
--

CREATE TABLE `t_keranjang` (
  `id` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `quantity` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_keranjang`
--

INSERT INTO `t_keranjang` (`id`, `id_produk`, `id_user`, `tanggal`, `quantity`) VALUES
(1, 1, 0, '2021-03-14 14:39:02', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_pelanggan`
--

CREATE TABLE `t_pelanggan` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `noktp` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `notelp` varchar(30) NOT NULL,
  `tgldaftar` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_pelanggan`
--

INSERT INTO `t_pelanggan` (`id`, `nama`, `username`, `password`, `noktp`, `alamat`, `notelp`, `tgldaftar`) VALUES
(7, 'uyung@gmail.com', 'uyung', '12345678', '123456789', 'padang', '23456789', '2021-03-07 14:03:05'),
(8, 'resti', 'resti@gmail.com', '123456', '123456789', 'padang', '345678', '2021-03-07 14:05:11'),
(9, 'dewi', 'dewi@gmail.com', '123456', '12345678', 'pesisir', '1234567', '2021-03-08 05:27:27'),
(10, 'dewi', 'dewi@gmail.com', '123456', '123456', 'kuranji', '123456', '2021-03-09 14:32:08'),
(11, 'deli', 'deli@gmail.com', '123456', '12345678', 'steba', '1234567', '2021-03-11 13:17:02'),
(12, 'witri', 'witri@gmail.com', '12345678', '123456', 'steba', '123456789', '2021-03-14 16:26:55'),
(13, 'dian', 'dian@gmail.com', '123456', '12345678', 'alai', '1234567890', '2021-03-15 01:06:32'),
(14, 'dani', 'dani@gmail.com', '123456', '123456789', 'steba', '082284816465', '2021-03-15 01:59:43'),
(15, 'nia', 'nia@gmail.com', '123456', '23456789', 'jati', '2345678', '2021-03-15 05:14:40'),
(16, 'deli', 'deli@gmail.com', '123456', '12345678', 'padang', '1234567', '2021-03-15 08:40:36');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_pesanan`
--

CREATE TABLE `t_pesanan` (
  `id` int(11) NOT NULL,
  `kd_booking` varchar(50) NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `id_paket` int(11) NOT NULL,
  `tempat` varchar(50) NOT NULL,
  `type_bayar` varchar(50) NOT NULL,
  `tgl_pesan` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tgl_booking` date NOT NULL,
  `pesan` text NOT NULL,
  `status` varchar(50) NOT NULL,
  `konfirmasi` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_pesanan`
--

INSERT INTO `t_pesanan` (`id`, `kd_booking`, `id_pelanggan`, `id_paket`, `tempat`, `type_bayar`, `tgl_pesan`, `tgl_booking`, `pesan`, `status`, `konfirmasi`) VALUES
(7, 'BKig210307030713', 8, 1, 'gedung', 'DP', '2021-03-07 14:47:46', '0000-00-00', '', 'booking', 'true'),
(8, 'BKig210307030823', 8, 1, 'gedung', 'Lunas', '2021-03-07 14:49:52', '2021-03-23', 'tambah meja dan kursi', 'bayar', 'true'),
(9, 'BKig210308062904', 9, 6, 'rumah', 'Lunas', '2021-03-08 05:31:54', '2021-03-18', '', 'bayar', 'false'),
(10, 'BKig210309033626', 9, 2, 'rumah', 'Lunas', '2021-03-09 14:40:49', '2021-03-22', '', 'bayar', 'true'),
(11, 'BKig210311021935', 11, 11, 'rumah', 'Lunas', '2021-03-11 13:22:39', '2021-03-25', '', 'bayar', 'false');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_produk`
--

CREATE TABLE `t_produk` (
  `id` int(11) NOT NULL,
  `kode` varchar(20) NOT NULL,
  `nama_produk` varchar(100) NOT NULL,
  `kategori` varchar(100) NOT NULL,
  `harga` double NOT NULL,
  `keterangan` text NOT NULL,
  `stok` int(3) NOT NULL,
  `dipakai` int(3) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_produk`
--

INSERT INTO `t_produk` (`id`, `kode`, `nama_produk`, `kategori`, `harga`, `keterangan`, `stok`, `dipakai`, `foto`) VALUES
(1, '', 'pelaminan luar rumah', 'lamin', 3500000, '<p>pelaminan luar rumah termasuk tempat duduk tamu pengantin</p>', 2, 1, '66608623_153778935744570_6579682283867561668_n.jpg'),
(2, '', 'pelaminan rumah', 'lamin', 3000000, '<p>pelaminan rumah bagian dalam</p>', 3, 0, '76019267659467_118643002502635_1940223215939407857_n.jpg'),
(3, '', 'tenda 01', 'tenda', 7500000, '<p>tenda gonjong</p>', 0, 0, '156890lamin.jpg'),
(4, '', 'tenda 02', 'tenda', 3000000, '<p>tenda petak 4x6</p>', 4, 0, '17294346700964_2166867200307831_1693404946612569266_n.jpg'),
(6, '', 'baju pengantin 1', 'baju', 1500000, '<p>baju pengantin satu set dengan sunting berwarna merah</p>', 4, 0, '65824064_2263894813717828_5284810859495976626_n.jpg'),
(7, '', 'baju pengantin 2', 'baju', 1500000, '<p>baju penganten satu set dengan sunting berwarna biru</p>', 4, 1, '62470727_336441673719813_4767314360061200398_n.jpg'),
(8, '', 'baju adat anak-anak', 'baju', 1000000, '<p>baju adat anak-anak perempuan</p>', 4, 0, '2593943700888_1933373970043505_5423247383643473308_n.jpg'),
(9, '', 'baju kebaya nikah', 'baju', 3500000, '<p>baju kebaya nikah sepasang&nbsp;</p>', 0, 0, '65881947_204418603842828_1450018947068170907_n.jpg'),
(10, '', 'songket 1 stel', 'baju', 3500000, '<p>songket satu stel</p>', 4, 0, '68807942880727_129689611334378_324758999602847417_n.jpg'),
(11, '', 'make up pengantin', 'makeup', 1000000, '<p>make up pengantin</p>', 4, 0, '73809864306142_426322668223942_6962484894429097986_n.jpg'),
(12, '', 'make up nikah ', 'makeup', 1000000, '<p>make up untuk pernikahan&nbsp;</p>', 4, 0, '31234738067043_2118545898220550_3926822146998796288_n.jpg'),
(13, '', 'make up wisuda', 'makeup', 250000, '<p>make up buat wisuda</p>', 4, 0, '17318741358715_1271843996288841_2417822721803891891_n.jpg'),
(16, '', 'paket 01', 'paket', 15000000, '<p>masuk semua atribut pelaminan,kecuali diluar tempat akat nikah yang tidak ada disediakan</p>', 4, 0, '60015845727544803108_139691520343535_1581169019635275672_n.jpg'),
(17, '', 'paket 02', 'paket', 20000000, '<p>termasuk semuanya telah disediakan sampai tempat akat nikah nya sekalian</p>', 4, 0, '76278676019267659467_118643002502635_1940223215939407857_n.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_transaksi`
--

CREATE TABLE `t_transaksi` (
  `id` int(11) NOT NULL,
  `id_pesanan` int(11) NOT NULL,
  `kd_booking` varchar(50) NOT NULL,
  `id_bank` int(11) NOT NULL,
  `bank_trans` varchar(100) NOT NULL,
  `norek_pengirim` varchar(50) NOT NULL,
  `nm_pengirim` varchar(50) NOT NULL,
  `jum_trans` double NOT NULL,
  `sisbay` double NOT NULL,
  `tgl_trans` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bukti` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_transaksi`
--

INSERT INTO `t_transaksi` (`id`, `id_pesanan`, `kd_booking`, `id_bank`, `bank_trans`, `norek_pengirim`, `nm_pengirim`, `jum_trans`, `sisbay`, `tgl_trans`, `bukti`) VALUES
(5, 8, 'BKig210307030823', 8, 'BRI', '554701014547532', 'NURUL', 3000000, 500000, '2021-03-09 17:00:00', '284118IMG20201028110052.jpg'),
(6, 9, 'BKig210308062904', 5, 'BRI', '554701014547532', 'dewi', 1500000, 0, '2021-03-13 17:00:00', '514587IMG20201130162737.jpg'),
(7, 10, 'BKig210309033626', 5, 'BRI', '554701014547532', 'dewi', 3000000, 0, '2021-03-19 17:00:00', ''),
(8, 11, 'BKig210311021935', 5, 'BRI', '554701014547532', 'deli', 10000000, -9000000, '2021-03-21 17:00:00', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_bank`
--
ALTER TABLE `t_bank`
  ADD PRIMARY KEY (`idbank`);

--
-- Indexes for table `t_checkout`
--
ALTER TABLE `t_checkout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_detailpesanan`
--
ALTER TABLE `t_detailpesanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_keranjang`
--
ALTER TABLE `t_keranjang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_pelanggan`
--
ALTER TABLE `t_pelanggan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_pesanan`
--
ALTER TABLE `t_pesanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_produk`
--
ALTER TABLE `t_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_transaksi`
--
ALTER TABLE `t_transaksi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_bank`
--
ALTER TABLE `t_bank`
  MODIFY `idbank` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `t_checkout`
--
ALTER TABLE `t_checkout`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `t_detailpesanan`
--
ALTER TABLE `t_detailpesanan`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `t_keranjang`
--
ALTER TABLE `t_keranjang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_pelanggan`
--
ALTER TABLE `t_pelanggan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `t_pesanan`
--
ALTER TABLE `t_pesanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `t_produk`
--
ALTER TABLE `t_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `t_transaksi`
--
ALTER TABLE `t_transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
