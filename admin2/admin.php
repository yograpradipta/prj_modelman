<!-- Page Header-->
<header class="page-header">
<div class="container-fluid">
    <h2 class="no-margin-bottom">Data Admin</h2>
</div>
</header>
<div class="container-fluid">
<p style="padding-top: 10px;">
    <a href="?page=admin&aksi=entri" class="btn btn-succes" style="background-color:green; color:white">Entri Admin</a>
    <a href="?page=admin&aksi=list" class="btn btn-primary">List Admin</a>
</p>
<section class="dashboard-counts no-padding-top">
    <?php
    include_once ("koneksi.php");
    $aksi=isset($_GET['aksi']) ? $_GET['aksi'] : 'list';
    switch ($aksi){
        case 'list' :
            ?>
        <div class="line-chart bg-white d-flex align-items-center justify-content-center has-shadow">
        <table  class="table table-striped">
            <thead>
                <tr>
                    <th><b>No</th>
                    <th><b>nama </th>
                    <th><b>username</th>
                    <th><b>password</th>
                    <th><b>notelp</th>
                    <th><b>Aksi</b></th>
                </tr>
            </thead>
            <?php
            $no=1;
            $tampil=mysql_query("SELECT * FROM admin ORDER BY id ASC");
            while($data=mysql_fetch_array($tampil)) {
                ?>
                <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $data['name']; ?></td>
                    <td><?php echo $data['username']; ?></td>
                    <td><?php echo $data['password']; ?></td>
                    <td><?php echo $data['notelp']; ?></td>
                    <td>
                      <?php
                          echo "<img src='foto_admin/$data[foto_admin]' width='80 height='40' hspace='10' border='0' align='left'>";
                          ?>
                    </td>
                    <td>
            						<a href="aksi_admin.php?page=admin&proses=hapus&id=<?php echo $data['id']; ?>"
            						   class="btn btn-danger btn-sm"
            						   onclick="return confirm('Yakin akan menghapus data ?');"><span class="glyphicon glyphicon-trash"></span>
            							Delete</a>
            						|
            						<a href="?page=admin&aksi=edit&id=<?php echo $data['id']; ?> " class="btn btn-warning btn-sm">
                          <span class="glyphicon glyphicon-edit"></span> Edit</a>
                    </td>
                </tr>
                <?php
                $no++;
           }
            ?>
        </table>
        </div>
        <?php
        break;

    case 'entri' :

        ?>

        <h2>Entri Admin</h2>

        <form action="aksi_admin.php?page=admin&proses=input" role="form" enctype="multipart/form-data" method="post">
            <div class="form-group">
                <label>nama</label>
                <input type="text" name="name" class="form-control" placeholder="nama admin">
                <label>username</label>
                <input type="text" name="username" class="form-control" placeholder="username">
                <label>password</label>
                <input type="text" name="password" class="form-control" placeholder="password">
                <label>nomor telepon</label>
                <input type="text" name="notelp" class="form-control" placeholder="notelp">
            </div>
            <div class="form-group">
                <label>foto</label>
                <input type="file" name="file_image">
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-default">Reset</button>
        </form>

        <?php
            break;
    case 'edit' :
        $ambil= mysql_query("SELECT * FROM admin WHERE id=$_GET[id]");
        $r=mysql_fetch_array($ambil);
        ?>

        <h2>Edit Data admin</h2>

        <form action="aksi_admin.php?page=admin&proses=update" role="form" enctype="multipart/form-data" method="post">
            <input type="hidden" name="id" value="<?php echo $r['id'];?>" class="form-control">

            <div class="form-group">
                <label>Nama</label>
                <input type="text" name="name" value="<?php echo $r['name']; ?>" class="form-control">
            </div>
            <div class="form-group">
                <label>username</label>
                <input type="text" name="username" value="<?php echo $r['username']; ?>" class="form-control">                
            </div>
            <div class="form-group">
                <label>password</label>
                <input type="text" name="password" value="<?php echo $r['password']; ?>" class="form-control">
            </div>
            <div class="form-group">
                <label>nomor telepon</label>
                <input type="text" name="notelp" value="<?php echo $r['notelp']; ?>" class="form-control">
            </div>
            <div class="form-group">
                <label>foto</label>
                <input type="file" name="file_image"> <br>

                <?php
                if ($r['foto_admin']!=''){
                    echo "<img src='foto_admin/$r[foto_admin]' width=100 height=50>";
                }
                else{
                    echo "tidak ada image";
                }
                ?>
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href="?page=admin">
            <button class="btn btn-warning" to="/admin">Batal</button></a>
        </form>
        <?php
        break;
	}
		?>
    </section>
</div>
