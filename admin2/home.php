<!-- Page Header-->
              <header class="page-header">
                <div class="container-fluid">
                  <h2 class="no-margin-bottom">Dashboard</h2>
                </div>
              </header>
<!-- Dashboard Counts Section-->
          <section class="dashboard-counts ">
            <div class="container-fluid">
              <div class="row bg-white has-shadow" align="center">
                <!-- Item -->
                <div class="col-xl-3 col-sm-6">
                  <div align="center">
                    <a href="index.php?page=pelaminan"> <img height=100px src="img/lamin.png"/>
				          	<br/><b>Pelaminan</b></a>
                  </div>
                </div>
                <!-- Item --> 
                <div class="col-xl-3 col-sm-6">
                  <div align="center">
                    <a href="index.php?page=tenda"> <img height=100px src="img/tenda.png"/>
					                 <br/><b>Tenda</b></a>
                  </div>
                </div>

                <!-- Item -->
                <div class="col-xl-3 col-sm-6">
                  <div align="center">
                    <a href="index.php?page=baju"> <img height=100px src="img/baju.png"/>
					                 <br/><b>Baju Pengantin</b></a>
                  </div>
                </div>
                <!-- Item -->
                <div class="col-xl-3 col-sm-6">
                  <div align="center">
                    <a href="index.php?page=makeup"> <img height=100px src="img/makeup.png"/>
					                 <br/><b>Make Up</b></a>
                  </div>
                </div>
              </div>
            </div>
          </section>
