<nav class="side-navbar">
          <!-- Sidebar Header-->
          <div class="sidebar-header d-flex align-items-center">
            <div class="avatar"><img src="img/user.png" alt="..." class="img-fluid rounded-circle"></div>
            <div class="title">
              <p>admin</p>
              <h1 class="h4"><?php echo $_SESSION['name'] ?></h1>
            </div>
          </div>
          <!-- Sidebar Navidation Menus-->
          <span class="heading">Menu</span>
          <ul class="list-unstyled">
            <li class="active"> <a href="index.php"><i class="icon-home"></i>Home</a></li>
			      
            <li >
              <a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse" class="collapsed">
              <i class="icon-interface-windows"></i>Menu </a>
              <ul  id="exampledropdownDropdown" class="list-unstyled collapse" style="">
                <li > <a href="?page=pelaminan"> <i class="fa fa-fort-awesome"></i>Pelaminan </a></li>
                <li><a href="?page=tenda"><i class="fa fa-flag"></i>Tenda </a></li>
                <li> <a href="?page=baju"> <i class="icon-user"></i>Baju Pengantin</a></li>
                <li><a href="?page=makeup"><i class="fa fa-paint-brush"></i>Make Up </a></li>
                <!-- <li><a href="?page=paket"><i class="fa fa-gift"></i>paket </a></li> -->
              </ul>
            
            </li>
            <li><a href="?page=bank"><i class="fa fa-university"></i>Bank </a></li>
            <li><a href="?page=pelanggan"><i class="fa fa-users"></i>Pelanggan </a></li>
            <li><a href="?page=pesanan"><i class="fa fa-shopping-cart"></i>Pesanan</a></li>
            <!-- <li><a href="?page=transaksi"><i class="fa fa-handshake-o"></i>Transaksi </a></li> -->
            <li><a href="?page=admin"><i class="fa fa-user-circle"></i>Admin </a></li>
            <li><a href="?page=komentar"><i class="fa fa-comment"></i>Komentar </a></li>
            <li> <a href="login.php"> <i class="icon-close"></i>Logout</a></li>
          </ul>
        </nav>
