<!-- Page Header-->
<header class="page-header">
<div class="container-fluid">
    <h2 class="no-margin-bottom">Data Tenda</h2>
</div>
</header>
<div class="container-fluid">
    <p style="padding-top: 10px;">
        <a href="?page=tenda&aksi=entri" class="btn btn-succes" style="background-color:green; color:white">Entri tenda</a>
        <a href="?page=tenda&aksi=list"  class="btn btn-primary">List tenda</a>
    </p>
    <section class="dashboard-counts no-padding-top">
        <?php
        include_once ("koneksi.php");
        $aksi=isset($_GET['aksi']) ? $_GET['aksi'] : 'list';
        switch ($aksi){
        case 'list' :
            ?>
            <div class="line-chart bg-white d-flex align-items-center justify-content-center has-shadow">
                <table  class="table table-striped ">
                    <thead>
                    <tr>
                        <th><b>No</th>
                        <th><b>nama tenda </th>
                        <th><b>harga</th>
                        <th><b>stok</th>
                        <th><b>keterangan</th>
                        <th><b>foto tenda</th>
                        <th><b>Aksi</b></th>
                    </tr>
                    </thead>
                    <?php
                    $no=1;
                    $tampil=mysql_query("SELECT * FROM t_produk where kategori='tenda' ORDER BY id ASC");
                    while($data=mysql_fetch_array($tampil)) {
                        ?>
                        <tbody>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo $data['nama_produk']; ?></td>
                            <td><?php echo $data['harga']; ?></td>
                            <td><?php echo $data['stok']; ?></td>
                            <td><?php echo $data['keterangan']; ?></td>
                            <td>
                            <?php
                                echo "<img src='foto_produk/$data[foto]' width='80 height='40' hspace='10' border='0' align='left'>";
                                ?>
                            </td>
                            <td>
                                            <a href="aksi_tenda.php?page=tenda&proses=hapus&id=<?php echo $data['id']; ?>"
                                            class="btn btn-danger btn-sm"
                                            onclick="return confirm('Yakin akan menghapus data ?');"><span class="glyphicon glyphicon-trash"></span>
                                                Delete</a>
                                            |
                                            <a href="?page=tenda&aksi=edit&id=<?php echo $data['id']; ?> " class="btn btn-warning btn-sm">
                                <span class="glyphicon glyphicon-edit"></span> Edit</a>
                            </td>
                        </tr>
                        </tbody>
                        <?php
                        $no++;
                }
                    ?>
                </table>
            </div>
            <?php
            break;

        case 'entri' :

            ?>

            <h2>Input Tenda</h2>

            <form action="aksi_tenda.php?page=tenda&proses=input" role="form" enctype="multipart/form-data" method="post">
                <div class="form-group">
                    <label>nama tenda</label>
                    <input type="text" name="nama_tenda" class="form-control" placeholder="nama tenda">
                    <label>Harga</label>
                    <input type="text" name="harga" class="form-control" placeholder="harga">
                    <label>Stok Tersedia</label>
                    <input type="text" name="stok" placeholder="masukan stok tersedia" class="form-control"> 
                    <label>Keterangan</label>
                    <textarea name="keterangan" class="form-control" placeholder="keterangan" id="keterangan"></textarea>
                </div>
                <div class="form-group">
                    <label>Gambar</label>
                    <input type="file" name="file_image">
                </div>
                <button type="submit" class="btn btn-primary">Simpan</button>
                <button type="reset" class="btn btn-warning">Reset</button>
            </form> <br>

            <?php
                break;
        case 'edit' :
            $ambil= mysql_query("SELECT * FROM t_produk WHERE id=$_GET[id]");
            $r=mysql_fetch_array($ambil);
            ?>

            <h2>Edit Data Tenda</h2>

            <form action="aksi_tenda.php?page=tenda&proses=update" role="form" enctype="multipart/form-data" method="post">
                <input type="hidden" name="id" value="<?php echo $r['id'];?>" class="form-control">

                <div class="form-group">
                    <label>Nama Tenda</label>
                    <input type="text" name="nama_tenda" value="<?php echo $r['nama_produk']; ?>" class="form-control">
                </div>
                <div class="form-group">
                    <label>Harga</label>
                    <input type="text" name="harga" value="<?php echo $r['harga']; ?>" class="form-control">                
                </div>
                <div class="form-group">
                <label>stok Tersedia</label>
                    <input type="text" name="stok" value="<?php echo $r['stok']; ?>" class="form-control">                
                </div>
                <div class="form-group">
                    <label>Keterangan</label>
                    <textarea name="keterangan" class="form-control" id="keterangan"><?php echo $r['keterangan']; ?></textarea>
                </div>
                <div class="form-group">
                    <label>Gambar</label>
                    <input type="file" name="file_image"> <br>

                    <?php
                    if ($r['foto']!=''){
                        echo "<img src='foto_produk/$r[foto]' width=100 height=50>";
                    }
                    else{
                        echo "tidak ada image";
                    }
                    ?>
                </div>
                <button type="submit" class="btn btn-primary">Simpan</button>
                <button type="reset" class="btn btn-warning">Reset</button>            
            </form><br>
            <?php
            break;
        }
            ?>
    </section>
</div>
