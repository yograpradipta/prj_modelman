<!-- Page Header-->
<header class="page-header">
    <div class="container-fluid">
        <h2 class="no-margin-bottom">Data Pesanan</h2>
    </div>
</header>
<div class="container">
    <p style="padding-top: 10px;">
        <a href="javascript:print('section-to-print')" class="btn btn-succes hide-to-print" style="background-color:green; color:white; width:80px" >
        <i class="fa fa-print"></i></a>
    </p>
    <section class="dashboard-counts no-padding-top">
        <?php
            include_once ("koneksi.php");
        ?>
            <div id="section-to-print">
                <div class="line-chart bg-white d-flex align-items-center justify-content-center has-shadow">
                    <table  class="table table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Booking </th>
                                <th>Tanggal Booking</th>
                                <th>nama pelanggan</th>
                                <th>status pesanan</th>
                                <th class="hide-to-print">Aksi</th>
                            </tr>
                        </thead>
                        <?php
                        $no=1;
                        $tampil=mysql_query("SELECT
                                t_checkout.*,
                                t_pelanggan.nama,
                                t_pelanggan.alamat
                                FROM t_checkout
                                LEFT JOIN t_pelanggan
                                ON t_pelanggan.id = t_checkout.id_pelanggan
                                ORDER BY t_checkout.id DESC");
                        while($data=mysql_fetch_array($tampil)) {
                            $now = "$data[tanggal]";
                            $limit = date ("Y-m-d h:i:s", strtotime("+1 day", strtotime($now)));
                            ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $data['kode_pesanan']; ?></td>
                                <td><?php echo $data['tgl_boking']; ?></td>
                                <td><?php echo $data['nama'];; ?></td>
                                <td><?php echo $data['status'];; ?></td>
                                <td class="hide-to-print">
                                    <a href="index.php?page=detail_pesan&kode=<?php echo $data['kode_pesanan']; ?>" class="btn btn-primary btn-sm">
                                    <span class="glyphicon glyphicon-trash"></span>
                                    Detail</a>
                                | <a href="aksi.php?page=pesanan&proses=hapus&id=<?php echo $data['id']; ?>" class="btn btn-danger btn-sm  hide-to-print">
                                 <i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            <?php
                            $no++;
                        };
                        ?>
                    </table>
                </div>
            </div>
    </section>
    <script>
        print(el) {
            var restorepage = document.body.innerHTML;
            var printcontent = document.getElementById(el).innerHTML;
            document.body.innerHTML = printcontent;
            window.print();
            document.body.innerHTML = restorepage;
        }
    </script>
</div>
