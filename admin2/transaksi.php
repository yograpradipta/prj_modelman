<!-- Page Header-->
<header class="page-header">
    <div class="container-fluid">
        <h2 class="no-margin-bottom">Data Transaksi</h2>
    </div>
</header>
<div class="container">
    <p style="padding-top: 10px;">
        <a href="javascript:print('section-to-print')" class="btn btn-succes hide-to-print" style="background-color:green; color:white; width:80px" >
        <i class="fa fa-print"></i></a>
    </p>
    <script>
        print(el){
            windows.print();
        };
    </script>
    <section class="dashboard-counts no-padding-top">
        <?php
            include_once ("koneksi.php");
        ?>
            <div class="line-chart bg-white d-flex align-items-center justify-content-center has-shadow">
                <table  class="table table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Pesanan </th>
                            <th>Nama Pengirim</th>
                            <th>Norek Pengirim</th>
                            <th>jumlah transfer</th>
                            <th>Tanggal Transfer</th>
                            <th class="hide-to-print">Aksi</th>
                        </tr>
                    </thead>
                    <?php
                    $no=1;
                    $tampil=mysql_query("SELECT t_transaksi.nm_pengirim, t_transaksi.norek_pengirim, 
                                        t_transaksi.jum_trans, t_transaksi.tgl_trans, t_pesanan.kd_booking,
                                         t_transaksi.id FROM t_transaksi 
                                         LEFT JOIN t_pesanan ON t_transaksi.id_pesanan = t_pesanan.id 
                                         ORDER BY t_transaksi.id DESC");
                    while($data=mysql_fetch_array($tampil)) {
                        ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo $data['kd_booking']; ?></td>
                            <td><?php echo $data['nm_pengirim']; ?></td>
                            <td><?php echo $data['norek_pengirim']; ?></td>
                            <td><?php echo $data['jum_trans']; ?></td>
                            <td><?php echo $data['tgl_trans']; ?></td>
                            <td class="hide-to-print">
                                <a href="index.php?page=detail_trans&kode=<?php echo $data['kd_booking']; ?>" class="btn btn-primary btn-sm">
                                <span class="glyphicon glyphicon-trash"></span>
                                Detail</a>
                            </td>
                        </tr>
                        <?php
                        $no++;
                    }
                    ?>
                </table>
            </div>
    </section>
</div>
