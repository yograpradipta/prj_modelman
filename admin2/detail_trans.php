<!-- Page Header-->
<header class="page-header">
    <div class="container-fluid">
        <h2 class="no-margin-bottom">Detail Transaksi</h2>
    </div>
</header>
<div class="container">
    <section >
        <?php
            include_once ("koneksi.php");
            $ambil= mysql_query("SELECT t_transaksi.nm_pengirim, t_transaksi.jum_trans,t_transaksi.norek_pengirim, 
                                t_transaksi.tgl_trans, t_bank.namabank,t_bank.atasnama,t_bank.norek,
                                t_pesanan.konfirmasi,t_transaksi.id_bank,t_transaksi.bank_trans,
                                t_transaksi.kd_booking,t_pesanan.tgl_pesan,t_produk.nama_produk, t_produk.harga,
                                t_pesanan.tgl_booking,t_pelanggan.nama,t_transaksi.sisbay,t_transaksi.bukti 
                                from t_transaksi 
                                LEFT JOIN t_pesanan on t_transaksi.id_pesanan=t_pesanan.id 
                                LEFT JOIN t_produk on t_pesanan.id_paket=t_produk.id 
                                LEFT JOIN t_pelanggan on t_pesanan.id_pelanggan=t_pelanggan.id 
                                LEFT JOIN t_bank on t_transaksi.id_bank = t_bank.idbank
                                WHERE t_transaksi.kd_booking='$_GET[kode]' ORDER BY t_transaksi.id DESC");
            $r=mysql_fetch_array($ambil);
        ?>
        <!-- start print -->
        <div id="section-to-print">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <table>
                                <tr>
                                    <td>Kode Pesanan</td>
                                    <td>: </td>
                                    <td><b>  <?php echo $r['kd_booking']; ?></b></td>
                                </tr>
                                <tr>
                                    <td>Tgl Pesanan</td>
                                    <td>: </td>
                                    <td><b>  <?php echo $r['tgl_booking']; ?></b></td>
                                </tr>
                            </table>
                        </div>
                                
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h4> Data Pengirim</h4>
                        </div>    
                        <div class="card-body">
                            <table>
                                <tr>
                                    <td>Nama Pengirim</td>
                                    <td>: </td>
                                    <td><b><?php echo $r['nm_pengirim']; ?> </b></td>
                                </tr>
                                <tr>
                                    <td>Nomor Rekening</td>
                                    <td>: </td>
                                    <td><b><?php echo $r['norek_pengirim']; ?> </b></td>
                                </tr>
                                <tr>
                                    <td>Bank</td>
                                    <td>: </td>
                                    <td><b><?php echo $r['bank_trans']; ?> </b></td>
                                </tr><tr>
                                    <td>Jumlah Transfer</td>
                                    <td>: </td>
                                    <td><b>RP.<?php echo $r['jum_trans']; ?>,- </b></td>
                                </tr>
                            </table>
                            
                        </div> 
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4> Data Bank Tujuan</h4>
                        </div>    
                        <div class="card-body">
                            <table>
                                <tr>
                                    <td>Bank Tujuan</td>
                                    <td>: </td>
                                    <td><b><?php echo $r['namabank']; ?> </b></td>
                                </tr>
                                <tr>
                                    <td>Nomor Rekening</td>
                                    <td>: </td>
                                    <td><b><?php echo $r['norek']; ?> </b></td>
                                </tr>
                                <tr>
                                    <td>status</td>
                                    <td>: </td>
                                    <td><b>Pesanan <?php if($r['konfirmasi']=='true'){
                                        echo 'telah Dikonfirmasi';
                                    }else if($r['konfirmasi']=='false'){
                                        echo 'belum dikonfirmasi';
                                    } ?> </b></td>
                                </tr>
                            </table>
                        </div> 
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h4> Data Pesanan</h4>
                        </div>    
                        <div class="card-body">
                            <table>
                                <tr>
                                    <td>Nama Pesanan</td>
                                    <td>: </td>
                                    <td><b><?php echo $r['nama_produk']; ?> </b></td>
                                </tr>
                                <tr>
                                    <td>Harga</td>
                                    <td>: </td>
                                    <td><b>Rp.<?php echo $r['harga']; ?>,- </b></td>
                                </tr>
                                <tr>
                                    <td>Waktu Booking</td>
                                    <td>: </td>
                                    <td><b><?php echo $r['tgl_booking']; ?> </b></td>
                                </tr>
                                <tr>
                                    <td>sisa bayar</td>
                                    <td>: </td>
                                    <td><b>Rp.<?php echo $r['sisbay']; ?> ,-</b></td>
                                </tr>
                                <tr style="vertical-align: top; ">
                                    <td style="padding-top:20px"><h3>Bukti Transaksi</h3></td>
                                    <td style="padding-top:20px">:</td>
                                    <td style="padding-top:20px">
                                        <img src="foto_bukti/<?php echo $r['bukti']; ?>" alt="foto bukti" width="300px"/>                               
                                    </td>
                                </tr>
                            </table>
                        </div> 
                    </div>
                </div>
            </div>
            
        </div>
            <!-- end print -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <a class="btn btn-rounded btn-danger hide-to-print " href="javascript:history.go(-1)">
                    <i class="icon-download"></i> back</a>  
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <a class="btn btn-rounded btn-primary hide-to-print " href="javascript:print('section-to-print')">
                    <i class="close-print"></i> print</a> 
                </div>
            </div>
            <script>
                // print(){
                //     windows.print();
                // },
                print(el) {
                    var restorepage = document.body.innerHTML;
                    var printcontent = document.getElementById(el).innerHTML;
                    document.body.innerHTML = printcontent;
                    window.print();
                    document.body.innerHTML = restorepage;
                }
            </script>
        </div>
    </section>
</div>