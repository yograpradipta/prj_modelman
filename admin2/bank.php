<!-- Page Header-->
<header class="page-header">
<div class="container-fluid">
    <h2 class="no-margin-bottom">Data Bank</h2>
</div>
</header>
<div class="container-fluid">
<p style="padding-top: 10px;">
    <a href="?page=bank&aksi=entri" class="btn btn-succes" style="background-color:green; color:white">Entri bank</a>
    <a href="?page=bank&aksi=list" class="btn btn-primary">List bank</a>
</p>
<section class="dashboard-counts no-padding-top">
    <?php
    include_once ("koneksi.php");
    $aksi=isset($_GET['aksi']) ? $_GET['aksi'] : 'list';
    switch ($aksi){
        case 'list' :
            ?>
            <div class="line-chart bg-white d-flex align-items-center justify-content-center has-shadow">
                <table  class="table table-striped">
                    <thead>
                        <tr>
                            <th><b>No</th>
                            <th><b>nama bank </th>
                            <th><b>Atas Nama</th>
                            <th><b>no rekening</th>
                            <th><b>foto bank</th>
                            <th><b>Aksi</b></th>
                        </tr>
                    </thead>
                    <?php
                    $no=1;
                    $tampil=mysql_query("SELECT * FROM t_bank ORDER BY idbank DESC");
                    while($data=mysql_fetch_array($tampil)) {
                        ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo $data['namabank']; ?></td>
                            <td><?php echo $data['atasnama']; ?></td>
                            <td><?php echo $data['norek']; ?></td>
                            <td>
                            <?php
                                echo "<img src='foto_bank/$data[logobank]' width='80 height='40' hspace='10' border='0' align='left'>";
                                ?>
                            </td>
                            <td>
                                            <a href="aksi_bank.php?page=bank&proses=hapus&id=<?php echo $data['idbank']; ?>"
                                            class="btn btn-danger btn-sm"
                                            onclick="return confirm('Yakin akan menghapus data ?');"><span class="glyphicon glyphicon-trash"></span>
                                                Delete</a>
                                            |
                                            <a href="?page=bank&aksi=edit&id=<?php echo $data['idbank']; ?> " class="btn btn-warning btn-sm">
                                <span class="glyphicon glyphicon-edit"></span> Edit</a>
                            </td>
                        </tr>
                        <?php
                        $no++;
                }
                    ?>
                </table>
            </div>
                <?php
                break;

    case 'entri' :

        ?>

        <h2>Entri Bank</h2>

        <form action="aksi_bank.php?page=bank&proses=input" role="form" enctype="multipart/form-data" method="post">
            <div class="form-group">
                <label>nama bank</label>
                <input type="text" name="namabank" class="form-control" placeholder="nama bank">
                <label>atas nama</label>
                <input type="text" name="atasnama" class="form-control" placeholder="atasnama">
                <label>no rekening</label>
                <input type="text" name="norek" class="form-control" placeholder="no rekening">
            </div>
            <div class="form-group">
                <label>Gambar</label>
                <input type="file" name="file_image">
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-default">Reset</button>
        </form>

        <?php
            break;
    case 'edit' :
        $ambil= mysql_query("SELECT * FROM t_bank WHERE idbank=$_GET[id]");
        $r=mysql_fetch_array($ambil);
        ?>

        <h2>Edit Data Bank</h2>

        <form action="aksi_bank.php?page=bank&proses=update" role="form" enctype="multipart/form-data" method="post">
            <input type="hidden" name="id" value="<?php echo $r['idbank'];?>" class="form-control">
            <div class="form-group">
                <label>nama bank</label>
                <input type="text" name="namabank" class="form-control" value="<?php echo $r['namabank'] ?>" placeholder="nama bank">
                <label>atas nama</label>
                <input type="text" name="atasnama" class="form-control" value="<?php echo $r['atasnama'] ?>" placeholder="atasnama">
                <label>no rekening</label>
                <input type="text" name="norek" class="form-control" value="<?php echo $r['norek'] ?>" placeholder="no rekening">
            </div>
            <div class="form-group">
                <label>Gambar</label>
                <input type="file" name="file_image"> <br>

                <?php
                if ($r['logobank']!=''){
                    echo "<img src='foto_bank/$r[logobank]' width=100 height=50>";
                }
                else{
                    echo "tidak ada image";
                }
                ?>
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href="?page=bank">
            <button class="btn btn-warning" to="/bank">Batal</button></a>
        </form>
        <?php
        break;
	}
		?>
    </section>
</div>
