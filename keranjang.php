<?php include_once "library/inc.library.php";?>
<section id="inner-headline">
    <div class="container">
    <div class="row">
        <div class="span4">
        <div class="inner-heading">
            <h2>Isi Keranjang</h2>
        </div>
        </div>
        <div class="span8">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home"></i></a><i class="icon-angle-right"></i></li>
            <li><a href="#">Isi Keranjang</a>
        </ul>
        </div>
    </div>
    </div>
</section>
<?php 
    $getData=mysql_query("SELECT t_keranjang.tanggal, t_keranjang.quantity, t_keranjang.id, 
        t_produk.nama_produk, t_produk.foto, t_produk.harga,t_produk.stok,t_produk.satuan,t_produk.foto 
        from t_keranjang 
        LEFT JOIN t_produk on t_keranjang.id_produk=t_produk.id 
        WHERE t_keranjang.id_user='$_SESSION[idmbr]' 
        ORDER BY t_keranjang.id DESC");
    ?>

<section id="content">
    <div class="container">
        <!-- Default table -->
        <div class="row">
            <form action="<?php $_SERVER['PHP_SELF']; ?>" method="post" name="form1" target="_self">
            <div class="span12">
            <!-- <h4>Daftar Pesanan</h4> -->
                <table class="table table-striped">
                <thead>
                    <tr>
                    <th>no</th>
                    <th>Gambar</th>
                    <th>Nama Item</th>
                    <th>Harga(Rp)</th>
                    <th>Banyak pesan</th>
                    <th>tersedia</th>
                    <th>satuan</th>
                    <th style="text-align:right;">total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $grandTotal = 0;
                        $no=1;
                        while($data=mysql_fetch_array($getData)){
                        $total = 0; 
                        $total=$data['quantity'] * $data['harga'];
                        $grandTotal=$grandTotal + $total;
                    ?>
                    <tr>
                        <td><?php echo $no ?></td>
                        <td><img src="admin2/foto_produk/<?php echo $data['foto']; ?>" width="100" border="1"></td>
                        <td><?php echo $data['nama_produk'] ?></td>
                        <td><?php echo format_angka($data['harga']) ?></td>
                        <td><input style="width:30px; margin-bottom:0; margin-right:8px" name="quantity[]" type="number" value="<?php echo $data['quantity'] ?>">
                            <input name="kode[]" type="hidden" value="<?php echo $data['id']; ?>">
                            <a class="btn btn-small btn-danger " href="aksi.php?page=keranjang&proses=hapus&idHapus=<?php echo $data['id'];?>">
                                <!-- <button >hapus</button> --> hapus
                            </a>
                        </td>
                        <td><?php echo $data['stok'] ?></td>
                        <td><?php echo $data['satuan'] ?></td>

                        <td style="text-align:right;"><?php echo format_angka($total) ?></td>
                    </tr>
                        <?php
                        $no++; 
                        }; ?>
                        
                    <tr style="text-align:right;">
                        <td style="background-color:#d2d4fe" colspan="7"><b>Grand Total</b></td>
                        <td  style="text-align:right; background-color:#d2d4fe"><b><?php echo format_angka($grandTotal) ?></b></td>
                    </tr>
                </tbody>
                </table>
          </div>
          <div class="span12" style="text-align:right">
                    <button  class="btn btn-large btn-info btn-rounded scrolldown " name="update">Update Data</button>
                    <a href="?page=checkout" class="btn btn-large btn-success btn-rounded scrolldown " name="pesan">Proses Pesanan</a>
            <?php
                if (isset($_POST['update'])) {
                    $arrData = count($_POST['quantity']); 
                    $qty = 1;
                    for ($i=0; $i < $arrData; $i++) {
                        # Melewati biar tidak 0 atau minus
                        if ($_POST['quantity'][$i] < 1) {
                            $qty = 1;
                            echo "<script>alert('masukan quantity lebih besar!')</script>";
                        }
                        else {
                            $qty = $_POST['quantity'][$i];
                        }
                            # Simpan Perubahan
                            $Kode	= $_POST['kode'][$i];
                            mysql_query("UPDATE t_keranjang set quantity = '$qty' 
                            where id='$Kode'");
                        
                    }
                    // Refresh
                    echo "<META HTTP-EQUIV='Refresh' Content='0; URL=?page=keranjang'>";
                    exit;
                }
            ?>
          </div>
          </form>
        
        </div>
    </div>
</section>