<section id="inner-headline">
    <div class="container">
    <div class="row">
        <div class="span4">
        <div class="inner-heading">
            <h2>Daftar Pesanan</h2>
        </div>
        </div>
        <div class="span8">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home"></i></a><i class="icon-angle-right"></i></li>
            <li><a href="#">Daftar pesanan</a>
        </ul>
        </div>
    </div>
    </div>
</section>
<?php 
    include_once "library/inc.library.php";    // Membuka librari peringah fungsi
    $getData=mysql_query("SELECT * FROM t_checkout 
                         WHERE id_pelanggan='$_SESSION[idmbr]'
                         ORDER BY id DESC");
    ?>

<section id="content">
    <div class="container">
        <!-- Default table -->
        <div class="row">
            <div class="span12">
            <!-- <h4>Daftar Pesanan</h4> -->
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>no</th>
                  <th>Kode Pesanan</th>
                  <th style="text-align:right">Total Bayar</th>
                  <th style="text-align:right">Dibayar</th>
                  <th style="text-align:right">Sisa Bayar</th>
                  <th>Tanggal Booking</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php
                    $no=1;
                    while($data=mysql_fetch_array($getData)){
                ?>
                <tr>
                    <td><?php echo $no ?></td>
                    <td><?php echo $data['kode_pesanan'] ?></td>
                    <td style="text-align:right"><?php echo format_angka($data['totbay']) ?></td>
                    <td style="text-align:right"><?php echo format_angka($data['jumlah_transfer']) ?></td>
                    <td style="text-align:right"><?php echo format_angka($data['sisbay']) ?></td>
                    <td><?php echo $data['tgl_boking'] ?></td>
                    <td><?php if($data['status']=='diproses'){
                        echo '<a href="#"><i class="icon-spinner"></i> diproses</a>';                          
                    }else if($data['status']=='cancel'){
                        echo '<a href="#"><i class="fa fa-times-circle"></i> ditolak</a>';  
                    }
                    else{
                        echo '<a href="#"><i class="icon-check"></i> diverifikasi</a>';  
                    } ?></td>
                    <td>
                        <a href="print.php?kode=<?php echo $data['kode_pesanan']; ?> " class="btn btn-theme btn-rounded">
                            <i class="fa fa-print" ></i> lihat detail
                        </a>
                    </td>
                </tr>
                    <?php
                    $no++; 
                    }; ?>
              </tbody>
            </table>
          </div>
        
        </div>
    </div>
</section>