<section id="inner-headline">
      <div class="container">
        <div class="row">
          <div class="span4">
            <div class="inner-heading">
              <h2>Tentang Kami</h2>
            </div>
          </div>
          <div class="span8">
            <ul class="breadcrumb">
              <li><a href="#"><i class="icon-home"></i></a><i class="icon-angle-right"></i></li>
              <li><a href="#">Pages</a><i class="icon-angle-right"></i></li>
              <li class="active">tentang</li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <section id="content">
      <div class="container">
        <div class="row">
          <div class="span6">
            <h2><strong>CV</strong>.Modelman</h2>
            <p>
                CV.Modelman adalah wedding organizer profesional yang berada di Padang Sumatera Barat. 
                Modelman sudah berdiri sejak 9 Oktober 2010,
                hingga sekarang telah meng-organize lebih dari 50 events. 
            </p>
            <p>
                Modelman menyediakan bebagai atribut pesta mulai dari Tenda, pelaminan, Baju pengantin hingga jasa Make UP 
                yang bisa dipesan terpisah.
            </p>
            <p>
                Modelman juga menyediakan baberapa paket lengkap yang bisa dilihat pada menu Paket!
            </p>
            <p>butuh informasi lebih bisa dihubungi pada kontak kami yang tersedia!</p>
          </div>
          <div class="span6">
            <!-- start flexslider -->
            <div class="flexslider">
              <ul class="slides">
                <li>
                  <img src="img/works/full/1.png" alt="" />
                </li>
                <li>
                  <img src="img/works/full/2.png" alt="" />
                </li>
                <li>
                  <img src="img/works/full/3.jpg" alt="" />
                </li>
                <li>
                  <img src="img/works/full/4.jpg" alt="" />
                </li>
                <li>
                  <img src="img/works/full/5.JPG" alt="" />
                </li>
                <li>
                  <img src="img/works/full/6.jpg" alt="" />
                </li>
              </ul>
            </div>
            <!-- end flexslider -->
          </div>
        </div>
      </div>
    </section>
    